/*
 * Menu.h
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#ifndef MENU_H_
#define MENU_H_

#include <iostream>
#include "Ksiazka.h"
#include "biblioteka.h"
#include <vector>
using namespace std;

class Menu {
public:
	void dodaj_ksiazke();
	void add_book_to_librrary(Ksiazka);
	Biblioteka biblioteka;
	Ksiazka search();
	Ksiazka wyszukaj_ksiazke();
	//void sprawdz_dostepnosc();
	void wyswietl_biblioteke();
	void dodaj_uzytkownika();
	void add_person_to_librrary(uzytkownik);
	Menu();
	virtual ~Menu();
	void borrow();
};

#endif /* MENU_H_ */
