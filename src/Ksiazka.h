/*
 * Ksiazka.h
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#ifndef KSIAZKA_H_
#define KSIAZKA_H_
#include "uzytkownik.h"
#include <iostream>
using namespace std;

class Ksiazka {
public:
	string tytul;
	string autor;
	int id;
	enum status {dostepny, wypozyczony, zarezerwowany, niedostepny};
	status danej_ksiazki;
	uzytkownik *wskaznik;

	Ksiazka(string="tytu�", string="autor");
	bool operator==(const Ksiazka& ksiazkax)const;
	virtual ~Ksiazka();
};

#endif /* KSIAZKA_H_ */
