/*
 * Biblioteka.h
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#ifndef BIBLIOTEKA_H_
#define BIBLIOTEKA_H_
#include <iostream>
#include "ksiazka.h"
#include "uzytkownik.h"
#include<vector>
using namespace std;

class Biblioteka {
public:
	void add_book_to_librrary(Ksiazka b);
	void add_person_to_librrary(uzytkownik b);
	vector<Ksiazka>library;
	vector<uzytkownik>pLibrary;
	int next_avaliable_id;
	Biblioteka();
	Ksiazka* find_book(Ksiazka ksiazka);
	virtual ~Biblioteka();
};


#endif /* BIBLIOTEKA_H_ */
