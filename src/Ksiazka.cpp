/*
 * Ksiazka.cpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#include "Ksiazka.h"

#include <iostream>
using namespace std;

Ksiazka::Ksiazka(string a, string t) {
	danej_ksiazki = dostepny;
	tytul = t;
	autor = a;
	cout << "Dodales do biblioteki nastepujaca pozycje: " << endl;
	cout << "autor:" << autor << " - " << "tytul: " << tytul << endl;
}

bool Ksiazka::operator==(const Ksiazka& ksiazkax) const {
	if (autor == ksiazkax.autor && tytul == ksiazkax.tytul)
		return true;
	return false;
}

Ksiazka::~Ksiazka() {

}

